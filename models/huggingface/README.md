# Huggingface Library (Open Models)

Playing around with the capabilities of the HF library focussing on Image Processing and Earth Observation.

The code is based on: https://www.deeplearning.ai/short-courses/open-source-models-hugging-face

## Examples
- Image Segmentation (SAM) on an aerial image of the new capital city of Indonesia (Nusantara)
- Object detection (with BB) on a travel image of the indonesian national dish Nasi Campur and an aerial image of bavaria of fields parcels and clouds (no success)
- Zero Shot Image classification (VIT) on a series of aerial Sentinel-2 images (urban, sea, fields/clouds) with good results