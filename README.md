# eoai-playground

**Collection for AI related scripts and material for Earth Observation and Remote Sensing**

![Qaqortoq in Greenland (Lat: 61.05862, Long: -46.04301) — Contains modified Copernicus Sentinel-2 false color data 2023](data/header.png)


## Resources

### General AI Online Courses

- NYU Deep Learning SP21 on [Youtube](https://www.youtube.com/playlist?list=PLLHTzKZzVU9e6xUfG10TkTWApKSZCzuBI) 
- NYU Deep Learning FL22 [Youtube](https://www.youtube.com/playlist?list=PLLHTzKZzVU9d_3TcHbyiAjl5qCbpJR-o0)
- MIT 6.S191: Introduction to Deep Learning on [Youtube](https://www.youtube.com/playlist?list=PLtBw6njQRU-rwp5__7C0oIVt26ZgjG9NI)
- Open source models with [Huggingface](https://www.deeplearning.ai/short-courses/open-source-models-hugging-face/?utm_campaign=huggingfaceC3-launch&utm_medium=headband&utm_source=dlai-homepage) on DeepLearning.AI

### General AI Books 

-  Hands-on Machine Learning with Scikit-Learn, Keras and TensorFlow (3rd edition) [Repo](https://github.com/ageron/handson-ml3)

## EO

### Libraries & Docs

- [Sentinelhub - Git](https://github.com/sentinel-hub/eo-flow) by Sentinelhub
- [Eo-Flow - Git](https://github.com/sentinel-hub/eo-flow) by Sentinelhub with [example notebooks](https://github.com/sentinel-hub/eo-flow/blob/master/examples/notebook.ipynb)
- [Huggingface ViT - Docs](https://huggingface.co/docs/transformers/model_doc/vit)
 - [TorchGeo - Docs](https://pytorch.org/blog/geospatial-deep-learning-with-torchgeo/) - domain library providing datasets, samplers, transforms, and pre-trained models specific to geospatial data - available on [Git](https://github.com/microsoft/torchgeo) - by Stewart, Adam J. and Robinson, Caleb and Corley, Isaac A. and Ortiz, Anthony and Lavista Ferres, Juan M. and Banerjee, Arindam 2022

### Case Studies & Notebooks

- LULC Slovenia LightGBM Notebook Sentinelhub [Notebook](https://github.com/sentinel-hub/eo-learn/blob/master/examples/land-cover-map/SI_LULC_pipeline.ipynb) and [Medium Articles](https://medium.com/sentinel-hub/land-cover-classification-with-eo-learn-part-2-bd9aa86f8500)
- [Image Classification with Hugging Face Transformers and `Keras` - Finetuning Google ViT for EuroSat](https://www.philschmid.de/image-classification-huggingface-transformers-keras)


### SOTA Methods in EO (Papers & Code)

- [Self-supervised pre-training for large-scale crop mapping using Sentinel-2 time series.](https://www.sciencedirect.com/science/article/abs/pii/S0924271623003386) by Xu, Yijia, Yuchi Ma, and Zhou Zhang 2024
- [The Pretrained Remote Sensing Transformer (Presto)](https://github.com/nasaharvest/presto) by Gabriel Tseng and Ruben Cartuyvels and Ivan Zvonkov and Mirali Purohit and David Rolnick and Hannah Kerner 2023
- [SatViT-1](https://ieeexplore.ieee.org/document/9866058) & [SatVit-2](https://arxiv.org/abs/2209.14969) - Code in [Git](https://github.com/antofuller/SatViT?tab=readme-ov-file) - by Fuller, Anthony and Millard, Koreen and Green, James R. 2022
- [TransformerEncoder - Self-Attention for Raw Optical Satellite Time Series Classification](https://github.com/MarcCoru/crop-type-mapping) by Marc Rußwurm and Marco Körner 2020
- [PSE+TEA - Satellite Image Time Series Classification with Pixel-Set Encoders and Temporal Self-Attention](https://github.com/VSainteuf/pytorch-psetae) by Sainte Fare Garnot, Vivien  and Landrieu, Loic and Giordano, Sebastien and Chehata, Nesrine 2020
- [Lightweight Temporal Self-Attention for Classifying Satellite Image Time Series](https://arxiv.org/abs/2007.00586) - Code in [Git](https://github.com/VSainteuf/lightweight-temporal-attention-pytorch?tab=readme-ov-file) - by Sainte Fare Garnot, Vivien  and Landrieu 2020
- [tempCNN - Temporal Convolutional Neural Network](https://github.com/charlotte-pel/temporalCNN) by Pelletier, Charlotte and Webb, Geoffrey I and Petitjean, Francois 2019


### Datasets

- [SBigEarthNet - A Large-Scale Sentinel Benchmark Archive](https://bigearth.net/) by G. Sumbul, A. d. Wall, T. Kreuziger, F. Marcelino, H. Costa, P. Benevides, M. Caetano, B. Demir, V. Markl 2021
- [AgriSen-COG, a Multicountry, Multitemporal Large-Scale Sentinel-2 Benchmark Dataset for Crop Mapping Using Deep Learning](https://www.mdpi.com/2072-4292/15/12/2980) by Teodora Selea 2023
- [Sentinel-Agri dataset](https://github.com/VSainteuf/pytorch-psetae) as used by the PSE+LTAE & the lightweight version by Garnot
- [EuroSAT: A Novel Dataset and Deep Learning
Benchmark for Land Use and Land Cover
Classification](https://arxiv.org/pdf/1709.00029v2.pdf) - available [here](https://paperswithcode.com/dataset/eurosat) - Sentinel-2 images with 13 spectral bands of 10 classes and a total count of 27k georeferenced images - by Helber et al. 2019:
    - [3 Bad RGB Dataset on HF](https://huggingface.co/datasets/blanchon/EuroSAT_RGB)
    - [12 Band MSI Dataset on HF](https://huggingface.co/datasets/blanchon/EuroSAT_MSI)
- Vast Collection of available datasets via [TorchGeo](https://torchgeo.readthedocs.io/en/stable/api/datasets.html#sentinel)
- GeoSpatial Datasets on [Huggingface](https://huggingface.co/collections/blanchon/geospatial-datasets-656ddb097c934a7b3c4d4619) by [Blanchon](https://huggingface.co/blanchon)